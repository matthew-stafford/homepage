package com.matthewstafford.homepage.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matthewstafford.homepage.models.Component;

public interface ComponentsRepository extends JpaRepository<Component, Long> {

}
