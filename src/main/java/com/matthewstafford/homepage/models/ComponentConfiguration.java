package com.matthewstafford.homepage.models;

import com.matthewstafford.homepage.enums.ComponentConfigKey;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "COMPONENT_CONFIGURATION")
public class ComponentConfiguration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "'KEY'")
	private ComponentConfigKey key;

	@Column(name = "'VALUE'")
	private String value;

	@ManyToOne
	@JoinColumn(name = "COMPONENT_ID", nullable = false)
	private Component component;

}
