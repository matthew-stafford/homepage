package com.matthewstafford.homepage.models;

import java.awt.Point;
import java.util.Set;

import com.matthewstafford.homepage.enums.ComponentType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "COMPONENT")
public class Component {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "WIDTH")
	private Byte width;

	@Column(name = "HEIGHT")
	private Byte height;

	@Column(name = "COMPONENT_TYPE")
	private ComponentType componentType;

	@Column(name = "CELL_POSITION")
	private Point cellPosition;

	@Column(name = "COMPONENT_CONFIG")
	@OneToMany(mappedBy = "component")
	private Set<ComponentConfiguration> componentConfiguration;

	@Column(name = "FAVICON")
	private byte[] favicon;

	@Column(name = "ICON")
	private byte[] icon;
}
