package com.matthewstafford.homepage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.matthewstafford.homepage.models.Component;
import com.matthewstafford.homepage.repositories.ComponentsRepository;

@RestController
@RequestMapping(value = "/api/components")
public class ComponentController {

	private ComponentsRepository componentRepository;

	@Autowired
	public ComponentController(ComponentsRepository componentRepository) {
		super();
		this.componentRepository = componentRepository;
	}

	@RequestMapping(value = "/{componentId}", method = RequestMethod.GET)
	public Component getComponentById(@PathVariable Long id) {
		return this.componentRepository.getReferenceById(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Component> getComponents() {
		return this.componentRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Component insertController(Component c) {
		return this.componentRepository.save(c);
	}
}
