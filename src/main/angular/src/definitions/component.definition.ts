import { CellPosition } from './cell-position.definition';
import { ComponentConfig } from './component-config.definition';
import { ComponentType } from './component-type.definition';

export interface Component {
  width: number;
  height: number;
  componentType: ComponentType;
  position: CellPosition;
  config?: ComponentConfig;
}
