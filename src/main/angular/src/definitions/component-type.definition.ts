export enum ComponentType {
  TOOLBAR = 'TOOLBAR',
  LINK = 'LINK',
  CPU_USAGE = 'CPU_USAGE',
  DISK_USAGE = 'DISK_USAGE',
  RAM_USAGE = 'RAM_USAGE',
}
