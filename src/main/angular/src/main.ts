import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app.config';
import { HomePageComponent } from './views/homepage/homepage.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
