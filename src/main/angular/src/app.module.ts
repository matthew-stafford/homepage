import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterOutlet } from '@angular/router';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HomePageComponent } from './views/homepage/homepage.component';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';

const PRIME_NG = [ToolbarModule, SplitButtonModule, InputTextModule];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterOutlet,
    ...PRIME_NG,
  ],
  declarations: [HomePageComponent, ToolbarComponent],
  bootstrap: [HomePageComponent],
})
export class AppModule {}
